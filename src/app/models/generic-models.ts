export class ConfigSite {
    jazzCoreApi: string;
    jazzApi: string;
    webConfig: WebConfigDto[];
}

export class WebConfigDto {
    name: string;
    imagesPath: string;
    menuOptions: string[];
    phone: string;
    netellerCode: string;
    ppFormEmail: string;
    ppFormURL: string;
    idSite: string;
    idAgent: number;
    idBook: string;
    language: string;
    landingImages: Array<any> = [];
    exteriorLineStyle: string;
    todayEventSport: string;
    landingPage: string;
}


