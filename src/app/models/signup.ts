export class SiteByPlayerForCountryCurrency {
    id: number;
    idSignUpColumn: number;
    name: string;
    idSignUpPlayer: number;
    mandatoryField: boolean;
    isActive: boolean;
    value: string;
    pathLanguage: string;
};

export class RequestSiteByPlayerForCountryCurrency {
    siteTag: string;
    idCountry: string;
    idCurrency: number;
};

export class Title {
    title: string;
};

export class Country {
    idCountry: string;
    country: string;
};

export class State {
    idISOCountry: string;
    idISOState: string;
    state: string;
};

export class Currency {
    idCurrency: number;
    currency: string;
    description: string;
};

export class CheckEmail {
    siteDomain: number;
    email: string;
};

export class CheckNameAddress {
    firstName: string;
    lastName: string;
    address: string;
    siteDomain: string;
};

export class GetParamsFromUrl {
    ipAddress: string;
    country: string;
    siteDomain: string;
};

export class CheckNamePhone {
    firstName: string;
    lastName: string;
    phone: string;
    siteDomain: string;
};

export class Player {
    password: string;
    name: string;
    lastName: string;
    lastName2: string;
    title: string;
    address1: string;
    address2: string;
    city: string;
    state: string;
    country: string;
    zip: string;
    phone: string;
    fax: string;
    email: string;
    dateOfBirth: Date;
    signUpIP: string;
    secQuestion: string;
    secAnswer: string;
    idUserSocialNetwork: string;
    idSignupPlayersType: number;
    idAgent?: number
};

export class FacebookUser {
    birthday: Date;
    email: string;
    first_name: string;
    gender: string;
    id: string;
    last_name: string;
};

export class GetToken {
    token: string;
};

export class ValidatePlayerInsert {
    firstName: string;
    lastName: string;
    address: string;
    siteDomain: string;
    phone: string;
    email: string;
    idLanguage: string;
    birthdate: Date;
}

export class PlayerInfo {
    idPlayer: number;
    master: boolean;
    status: string;
    onlineAccess: boolean;
    eposPlayer: boolean;
    creditLimit: number;
    chartPercent: number;
    masterChart: number;
    idLineType: number;
    idCurrency: number;
    idOffice: number;
    idGrouping: number;
    idPlayerRate: number;
    playerRateName: string;
    idBook: number;
    idProfile: number;
    idProfileLimits: number;
    idAgent: number;
    maxWager: number;
    minWager: number;
    onlineMaxWager: number;
    onlineMinWager: number;
    holdBets: boolean;
    holdDelay: number;
    enableSports: boolean;
    enableCasino: boolean;
    enableHorses: boolean;
    enableCards: boolean;
    showInTicker: boolean;
    lineStyle: string;
    nhlLine: string;
    mlbLine: string;
    pitcherDefault: number;
    scheduleStyle: string;
    idTimeZone: number;
    idLanguage: number;
    player: string;
    country: string;
    lastModificationUser: number;
    currency: string;
};

export class WelcomePlayerParams {
    title: string;
    player: string;
    password: string;
    name: string;
    lastName: string;
    idLanguage: number;
};

export class infoEmail {
    message: String;
    status: number;
};

export class LocationService {
    ip: string;
    city: string;
    region: string;
    country: string;
    loc: string;
    postal: string;
    org: string;
};

export class SendHash {
    hash: string;
};

export class GetSite {
    siteDomain: string;
    player: string;
    password: string;
};

export class GetBook {
    bookDescription: string;
};

export class playerIdentificationInsert {
    idPlayer: number;
    idNumber: string;
    passport: string;
    rfc: string;
    curp: string;
};

export class insertCsiPlayer {
    accountNumber: string;
    book: string;
    firstName: string;
    lastName: string;
    ipAddress: string;
    address: string;
    city: string;
    state: string;
    zip: string;
    country: string;
    homePhone: string;
    workPhone: string;
    mobilePhone: string;
    fax: string;
    textMessages: boolean;
    email: string;
    hear: string;
    mother: string;
    ssn: string;
    remarks: string;
    referralAccount: string;
    bankingInfo: string;
    sports: boolean;
    casino: boolean;
    horses: boolean;
    poker: boolean;
    userId: number;
    password: string;
    feesOver: number;
    promoCode: string;
    claimedBy: number;
}

export class socialNetworksBySiteTag {
    idSocialNetworkTokenBySiteTag: number;
    idSignupPlayerType: number;
    socialNetworkName: string;
    token: string;
    siteTag: {
        idSiteTag: number;
        description: string;
        siteDomain: string;
    };
}

export class affiliateCookie {
    affiliateCode: string;
    bannerCode: string;
    campaignCode: string;
}

export class termsConditions {
    id: number;
    idSignupPlayers: number;
    pathLanguage: string;
    isActive: boolean;
    lastModifiedBy: number;
}

export class UrlImages {
    urls: string;
}

export class ShowMessageText {
    message: string;
    idPartMessage: number;
};


export class agent {
    idAgent: number;
    agent: string;
    distribuitor: number;
    password: string;
    enable: boolean;
    isDistribuitor: boolean;
    lastModification: Date;
    email: string;
}

export class affiliateAgent {
    idAgent: number;
    Agent: string;
    email: string;
    affiliateCode: string;
    masterPlayer: string;
}