import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgentsSignUpComponent } from '././components/agents/agents-sign-up/agents-sign-up.component';
// import { WelcomePlayerComponent } from './components/players/welcome-player/welcome-player.component';
import { EmailConfirmationComponent } from './components/agents/email-confirmation/email-confirmation.component'
import { AppComponent } from './app.component';
// import { SignupDealerSportsBookComponent } from './components/players/signup-dealer-sports-book/signup-dealer-sports-book.component';
const routes: Routes = [
    {
        path: 'home', component: AppComponent, children: [
            // { path: '', redirectTo: '/landing', pathMatch: 'full' },
            { path: 'Signup', component: AgentsSignUpComponent },
            // { path: 'welcome-player', component: WelcomePlayerComponent },
            { path: 'confirm-account', component: EmailConfirmationComponent },
            // { path: 'signup-dealersportsbook', component: SignupDealerSportsBookComponent },
        ]
    },
    {
        path: 'Inicio', component: AppComponent, children: [
            { path: 'registro', component: AgentsSignUpComponent },
            { path: 'confirmar-cuenta', component: EmailConfirmationComponent },
            // { path: 'bienvenido-jugador', component: WelcomePlayerComponent },
            // { path: 'registro-dealersportsbook', component: SignupDealerSportsBookComponent },
        ]
    },
];

export const AppRoutingModule = RouterModule.forRoot(routes, { initialNavigation: true });
