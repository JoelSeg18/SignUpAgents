import { Injectable } from "@angular/core";
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
//import { ToastrService }          from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class InterceptorHttp implements HttpInterceptor {


    constructor(private router: Router, private route: ActivatedRoute, private translate: TranslateService) {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
       
        let request = req.clone({
            setHeaders: {
                //'Content-Type': 'application/json'
            }
        });
        console.log("holamundo", next)
        return next.handle(request).catch(
            (err: HttpErrorResponse) => {
                if (err.status == 401) {
                    console.log(err);
                    localStorage.clear();
                    return Observable.throw('Not authorized');
                }
                console.log(err);
                return Observable.throw('Error');
            }
        );
    }
}