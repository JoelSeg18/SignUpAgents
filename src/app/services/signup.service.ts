import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import 'rxjs/Rx';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { SiteByPlayerForCountryCurrency, RequestSiteByPlayerForCountryCurrency, Country, Player, FacebookUser, GetToken, Currency, CheckEmail, CheckNamePhone, CheckNameAddress, ValidatePlayerInsert, PlayerInfo, WelcomePlayerParams, State, infoEmail, LocationService, SendHash, GetSite, GetBook, playerIdentificationInsert, insertCsiPlayer, socialNetworksBySiteTag, termsConditions, UrlImages, ShowMessageText, agent, affiliateAgent, affiliateCookie } from '../models/signup';
import { sha256 } from 'js-sha256';


@Injectable({
  providedIn: 'root'
})
export class SignupService {

  private characters: string[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']


  constructor(private _http: HttpClient) { }

  private GetHeaders(ip: string, siteDomain: string): HttpHeaders {

    var token: string = sha256(ip + ":&:" + siteDomain);

    console.log(token);
    var x = window.location.hostname;

    var encode = btoa(this.characters[(Math.floor(Math.random() * (35 - 0)) + 0)] + token + this.characters[(Math.floor(Math.random() * (35 - 0)) + 0)]);

    return new HttpHeaders({ 'Authorization': 'bearer ' + encode });
  }

  getCurrentLocation(apiUrl: string, token: string): Observable<any> {
    return this._http.get(apiUrl + token)
      .map((resp: any) => <any>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  }

  getColumnsBySiteCountryCurrency(apiUrl: string, requestColumnsPlayer: RequestSiteByPlayerForCountryCurrency): Observable<SiteByPlayerForCountryCurrency[]> {
    let apiMethod: string = 'SignUpAdministrator/ColumnsBySitePlayerCountryCurrency';
    return this._http.post(apiUrl + apiMethod, requestColumnsPlayer)
      .map((resp: SiteByPlayerForCountryCurrency[]) => <SiteByPlayerForCountryCurrency[]>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  }

  getTitle(): Observable<any> {
    return this._http.get("../../assets/title.json")
      .map((response: Response) => <any>response)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  }

  getCountry(apiUrl: string): Observable<Country[]> {
    return this._http.get(apiUrl + 'SignUpAdministrator/getCountryForSignUp')
      .map((resp: any) => <Country[]>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  getCurrenciesBySiteAndCountry(apiUrl: string, siteDomain: string, idCountry: string): Observable<Currency[]> {
    return this._http.get(apiUrl + 'SignUpAdministrator/getCurrenciesBySiteDomainCountry?siteDomain=' + siteDomain + '&idCountry=' + idCountry)
      .map((resp: Currency[]) => <Currency[]>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  playerInsert(apiUrl: string, req): Observable<PlayerInfo> {
    let apiMethod: string = 'SignUpAdministrator/SignUpPlayerInsert';
    return this._http.post(apiUrl + apiMethod, req, { headers: this.GetHeaders(req.playerInsert.signUpIP, req.reqSignUp.siteDomain) })
      .map((resp: PlayerInfo) => <PlayerInfo>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  getFacebookData(token: string): Observable<FacebookUser> {
    return this._http.get('https://graph.facebook.com/v3.2/me?fields=id%2C%20birthday%2C%20location%2C%20first_name%2C%20last_name%2C%20email%2C%20gender&access_token=' + token + '')
      .map((resp: any) => <FacebookUser>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  checkEmailBySignUpGroup(apiUrl: string, siteDomain: string, email: string): Observable<CheckEmail[]> {
    return this._http.get(apiUrl + 'SignUpAdministrator/checkEmailBySignupGroup?siteDomain=' + siteDomain + '&email=' + email)
      .map((resp: CheckEmail[]) => <CheckEmail[]>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  checkNameAddressBySignupGroup(apiUrl: string, firstName: string, lastName: string, address: string, siteDomain: string): Observable<CheckNameAddress[]> {
    return this._http.get(apiUrl + 'SignUpAdministrator/checkNameAddressBySignupGroup?firstName=' + firstName + '&lastName=' + lastName + '&address=' + address + '&siteDomain=' + siteDomain)
      .map((resp: CheckNameAddress[]) => <CheckNameAddress[]>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  checkNamePhoneBySignupGroup(apiUrl: string, firstName: string, lastName: string, phone: string, siteDomain: string): Observable<CheckNamePhone[]> {
    return this._http.get(apiUrl + 'SignUpAdministrator/checkNameAddressBySignupGroup?firstName=' + firstName + '&lastName=' + lastName + '&phone=' + phone + '&siteDomain=' + siteDomain)
      .map((resp: CheckNamePhone[]) => <CheckNamePhone[]>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  validatePlayerInsert(apiUrl: string, validatePlayerInsert: ValidatePlayerInsert): Observable<string> {
    let apiMethod: string = 'SignUpAdministrator/ValidatePlayerInsert';
    return this._http.post(apiUrl + apiMethod, validatePlayerInsert)
      .map((resp: string) => <string>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  }

  getPlayerForChargeMessage(apiUrl: string, idPlayer: number): Observable<WelcomePlayerParams> {

    console.log('GO ==>', idPlayer);

    return this._http.get(apiUrl + 'SignUpAdministrator/GetPlayerForMessage?idPlayer=' + idPlayer)
      .map((resp: WelcomePlayerParams) => <WelcomePlayerParams>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  getState(apiUrl: string, idCountry: string): Observable<State[]> {
    return this._http.get(apiUrl + 'SignUpAdministrator/GetCountryStates?idCountry=' + idCountry)
      .map((resp: any) => <State[]>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  activeAccount(apiUrl: string, hash: String): Observable<infoEmail> {
    return this._http.get(apiUrl + "SignUpAdministrator/confirmAccount?hash=" + hash)
      .map((resp: any) => <infoEmail>resp)
      .catch((error: any) => Observable.throw(error || 'Server error'));
  };

  getSiteWithHash(apiUrl: string, hash: String): Observable<GetSite> {
    return this._http.get(apiUrl + 'SignUpAdministrator/GetSiteByHash?hash=' + hash)
      .map((resp: any) => <GetSite>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  sendIpAddress(apiUrl: string): Observable<any> {
    return this._http.get(apiUrl + 'captureIp.rest.php')
      .map((resp: any) => {
        return <any[]>resp;
      })
      .catch((error: any) => {
        return Observable.throw(error || 'Server Error')
      });
  };

  getBookName(apiUrl: string, idBook: number): Observable<GetBook> {
    return this._http.get(apiUrl + "SignUpAdministrator/GetBookName?idBook=" + idBook)
      .map((resp: any) => <GetBook>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  playerIdentificationInsert(apiUrl: string, player: playerIdentificationInsert): Observable<boolean> {
    return this._http.post(apiUrl + "SignUpAdministrator/playerIdentificationInsert", player)
      .map((resp: boolean) => <boolean>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  insertCsiPlayer(apiUrl: string, req: insertCsiPlayer): Observable<any> {
    return this._http.post(apiUrl + 'SignUpAdministrator/insertCsiPlayer', req)
      .map((resp: any) => <any[]>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  getsocialNetworkToken(apiUrl: string, req: string): Observable<socialNetworksBySiteTag[]> {
    return this._http.get(apiUrl + 'SignUpAdministrator/getSocialNetworkTokensBySiteTag?siteDomain=' + req)
      .map((resp: socialNetworksBySiteTag[]) => <socialNetworksBySiteTag[]>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  }

  getTermsConditions(apiUrl: string, req: number): Observable<termsConditions> {
    console.log(apiUrl + "getTermsConditionsSignupPlayers?idSignupPlayer=" + req)
    return this._http.get(apiUrl + "SignUpAdministrator/getTermsConditionsSignupPlayers?idSignupPlayer=" + req)
      .map((resp: termsConditions) => <termsConditions>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  }

  getImagesBySite(apiUrl: string, idSite: number): Observable<UrlImages[]> {
    return this._http.get(apiUrl + 'SignUpAdministrator/getImagesBySite?idSite=' + idSite)
      .map((resp: UrlImages[]) => <UrlImages[]>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  }

  getIdSite(apiUrl: string, urlSite: string): Observable<number> {
    return this._http.get(apiUrl + 'SignUpAdministrator/getIdSiteDomain?siteDomain=' + urlSite)
      .map((resp: number) => <number>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  }

  getMessageByUrlSiteLanguage(apiUrl: string, Site: string, idLanguage: number, idMessageType: number): Observable<ShowMessageText[]> {
    return this._http.get(apiUrl + "SignUpAdministrator/getMessageByUrlSiteLanguage?Site=" + Site + "&idLanguage=" + idLanguage + "&idMessageType=" + idMessageType)
      .map((resp: any) => <ShowMessageText[]>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  resendEmailConfirmation(apiUrl: string, req: { email: string; siteDomain: string; idLanguage: string }): Observable<boolean> {
    return this._http.post(apiUrl + 'SignUpAdministrator/generateEmailConfirmation', req)
      .map((resp: boolean) => <boolean>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  }

  getAffiliatesAgents(apiUrl: string, agent: agent): Observable<affiliateAgent[]> {
    return this._http.post(apiUrl + 'temp/GetAffiliateList', agent)
      .map((resp: affiliateAgent[]) => <affiliateAgent[]>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  getDealerSportbookValidatePhone(apiUrl: string, phone: string): Observable<boolean> {
    return this._http.get(apiUrl + 'SignUpAdministrator/dealerSportbookValidatePhone?phone=' + phone)
      .map((resp: boolean) => <boolean>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  }

  playerInsertDealerSportsBook(apiUrl: string, req): Observable<PlayerInfo> {
    let apiMethod: string = 'SignUpAdministrator/SignUpPlayerInsertDealerSportsBook';
    return this._http.post(apiUrl + apiMethod, req)
      .map((resp: PlayerInfo) => <PlayerInfo>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  getAffiliates(apiUrl: string, req: affiliateCookie): Observable<affiliateAgent> {
    return this._http.post(apiUrl + 'SignUpAdministrator/getAffiliate', req)
      .map((resp: affiliateAgent) => <affiliateAgent>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };

  SendImages(apiUrl: string, req: GetSite): Observable<boolean> {
    return this._http.post(apiUrl + 'SignUpAdministrator/GetImageDataForNotification', req)
      .map((resp: boolean) => <boolean>resp)
      .catch((error: any) => Observable.throw(error || 'Server Error'));
  };


}
