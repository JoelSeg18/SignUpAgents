import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ConfigSite } from '../models/generic-models';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(private _http: HttpClient) { }

  getConfig(): Observable<ConfigSite> {
    return this._http.get("../../assets/config.json?v=1.0")
      .map((response: ConfigSite) => <ConfigSite>response)
      .catch((error: any) => Observable.throwError(error || 'Server Error'));
  }

}
