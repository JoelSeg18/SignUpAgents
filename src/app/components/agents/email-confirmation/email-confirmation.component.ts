import { Component, OnInit } from '@angular/core';
import { ConfigSite } from '../../../models/generic-models';
import { GetSite } from '../../../models/signup';
import { ConfigService } from '../../../services/config.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SignupService } from '../../../services/signup.service';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-email-confirmation',
  templateUrl: './email-confirmation.component.html',
  styleUrls: ['./email-confirmation.component.css']
})
export class EmailConfirmationComponent implements OnInit {

  //#region variables
  _configData: ConfigSite;
  public _sendHash: string;
  public _confirmPass: string;
  public _siteDomain: string;
  public hash: string;
  public _model: any = {};
  public routing: string;
  public getSite: GetSite = new GetSite();
  loading: boolean = false;
  // #endregion variables

  constructor(
    private config: ConfigService
    , private router: ActivatedRoute
    , private signupService: SignupService
    , private toastr: ToastrService
    , private location: Location
    , private route: Router
    , private _translate: TranslateService
  ) {
    this.router.queryParams
      .subscribe(
        param => {
          this.hash = param["h"];
        },
        error => {
          console.log("Error contructor");
        }
      );

    route.events
      .subscribe(
        (val) => {
          if (location.path() != '') {
            this.routing = location.path();
          } else {
            this.routing = ''
          }
        });
  }

  ngOnInit() {
    this.loading = true;
    this.config.getConfig().subscribe(
      resp => {
        this._configData = resp;
      },
      error => {
        console.log('error get config');
      },
      () => {
        this.activeAccount();
      }
    )
  }

  activeAccount() {
    try {
      this._sendHash = this.hash;
      this.signupService.getSiteWithHash(this._configData.jazzCoreApi, this._sendHash)
        .subscribe(
          resp => {
            if (resp) {
              this.getSite = resp;
              this.signupService.SendImages(this._configData.jazzCoreApi, this.getSite).subscribe(
                resp => {
                  if (resp = true) {
                   console.log("se envio el Email");
                  }else{
                    console.log("no se envio el Email");
                  }
            });
          }
          },
          error => {
          }
        );
      this.signupService.activeAccount(this._configData.jazzCoreApi, this.hash)
        .subscribe(
          resp => {
            this._model.infoEmail = resp;
            this._model.isBusy = false;
            if (this._model.infoEmail.status == 0) {
              this._model.previousActivated = true;
            }
            else if (this._model.infoEmail.status == 1) {
              this._model.newActivated = true;
            } else if (this._model.infoEmail.status == 2) {
              this._model.errorActivated = true;
            }
            this.loading = false;
          }, Error => {
            this.loading = false;
            this._model.isBusy = false;
            this.toastr.error(this._translate.instant("signupMessages.errorActivateAccount"), this._translate.instant("signupMessages.error"));
          }
        );
    } catch (error) {
      this.loading = false;
      this._model.isBusy = false;
      this.toastr.warning(this._translate.instant("signupMessages.errorActivateAccount"), this._translate.instant("signupMessages.error"));
    }
  }

}
