import { Component, Inject, OnInit } from '@angular/core';
import { ConfigService } from '../../../services/config.service';
import { ConfigSite } from 'src/app/models/generic-models';
import { SiteByPlayerForCountryCurrency, Country, Player, FacebookUser, Currency, ValidatePlayerInsert, PlayerInfo, State, LocationService, playerIdentificationInsert, insertCsiPlayer, affiliateCookie, Title, termsConditions, UrlImages } from '../../../models/signup';
import { ToastrService } from 'ngx-toastr';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { SignupService } from 'src/app/services/signup.service';
import { TranslateService } from '@ngx-translate/core';
import { SocialUser, AuthService, AuthServiceConfig } from 'angularx-social-login';
import { NgForm } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { repeat } from 'rxjs/operators';
// import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-agents-sign-up',
  templateUrl: './agents-sign-up.component.html',
  styleUrls: ['./agents-sign-up.component.css']
})
export class AgentsSignUpComponent implements OnInit {

   //#region variables
   _configData: ConfigSite;
   _siteByPlayerForCountryCurrency: SiteByPlayerForCountryCurrency[] = [];
   _currency: Currency[];
   _title: Title[];
   _country: Country[];
   _state: State[];
   _selectedTitle: string;
   _player: Player;
   _ConfirmPass: string;
   _titleSelected: string;
   _countrySelected: string;
   _stateSelected: string;
   _dateOfBith: Date;
   _email: string;
   _token: string;
   _facebookUser: FacebookUser;
   _titleName: string;
   _birthday: Date;
   _siteDomain: string;
   _cloudCountry: string;
   _domain: string;
   _ipAddress: string;
   _currencySelected: Currency;
   currencyVisible: boolean;
   player: Player;
   validatePlayerInsert: ValidatePlayerInsert;
   _playerInfo: PlayerInfo;
   _anonymousAccount: boolean = false;
   state: State;
   siteTheme: string;
   date: Date = new Date();
   _location: LocationService;
   _postal: string;
   _playerIdentification: playerIdentificationInsert;
   _insertCsiPlayer: insertCsiPlayer;
   poker: boolean;
   casino: boolean;
   racebook: boolean;
   sportsBook: boolean;
   passwValid: boolean;
   emailValid: boolean;
  //  facebookLoginProvider: FacebookLoginProvider;
  //  googleLoginProvider: GoogleLoginProvider;
   user: SocialUser;
   emailAA: string;
   emailAAConfirm: string;
   emailAAValid: boolean;
   authService: AuthService;
   affiliateCookie: affiliateCookie;
   hearVisible: boolean = false;
   interestsVisible: boolean = false;
   termsConditionsVisible: boolean = false;
   termsConditions: any;
   acceptTermsConditions: boolean = false;
   apiLocationIntents: number;
   _urlImages: UrlImages[];
   loading: boolean;
   _resendEmailVisible: boolean = false;
   _emailToResend: string;
   sendingInfo: boolean = false;
   currentLang: string;

   _today = new Date();
   _password2 = '';
   //#endregion Variables

   constructor(
     private config: ConfigService,
     private signupService: SignupService,
     @Inject(DOCUMENT) private document: any,
     private toastr: ToastrService,
     private route: ActivatedRoute,
     private router: Router,
     private translate: TranslateService,
     private cookieService: CookieService,
    //  private datePipe: DatePipe
   ) {
     this.loading = true;
    //  this._today = this.datePipe.transform(this._today, 'yyyy-MM-dd');
     this.route.queryParams
       .subscribe(
         resp => {
           this._domain = resp['domain'];
           this.currentLang = resp ['lang'];
           this.translate.use(this.currentLang)
           if (this._domain.includes('https://')) {
             this._domain = this._domain.split("https://")[1];
           }
           else if (this._domain.includes("http://")) {
             this._domain = this._domain.split("http://")[1];
           }
         }
       );
   };

   ngOnInit() {
     try {
       this.config.getConfig().subscribe(
         resp => {
           this._configData = resp;
           this.apiLocationIntents = 0;
           console.log(this._siteByPlayerForCountryCurrency);
         },
         error => {
           console.log('Error get config');
         },
         () => {
           try {
             this.getCurrentLocation();
             this.getIdSiteByUrl();
           } catch{

           }

         }
       );
     } catch{
       console.log("Error get config");
     }

   }

   //#region get


   apiLocation(apiLocation: { apiUrl: string; apiToken: string }) {
     this.signupService.getCurrentLocation(apiLocation.apiUrl, apiLocation.apiToken)
       .subscribe(
         resp => {
           if (resp.error) {
             this.apiLocationIntents++;
             this.getCurrentLocation();
           } else if (this.apiLocationIntents < 3) {
             this._location = resp;
           } else if (this.apiLocationIntents == 3) {
             this._location = {
               ip: resp.ip,
               city: resp.city,
               loc: resp.region,
               country: resp.country_code,
               postal: resp.postal,
               org: null,
               region: resp.region_code
             }
           }
         },
         error => {
           if (this.apiLocationIntents < 4) {
             this.apiLocationIntents++;
             this.getCurrentLocation();
           }
         },
         () => {
           if (this.apiLocationIntents < 4) {
             this._cloudCountry = this._location.country;
             this._ipAddress = this._location.ip;
             this._postal = this._location.postal;

           }
           this.getCurrency();
           this.getTitle();
           this.getCountry();
           this.getState(false);
           this.affiliateCookie = {
             affiliateCode: this.cookieService.get("affiliateCode") || null,
             bannerCode: this.cookieService.get("bannerCode") || null,
             campaignCode: this.cookieService.get("campaignCode") || null
           }
          //  this.signupService.getsocialNetworkToken(this._configData.jazzCoreApi, this._domain)
          //    .subscribe(
          //      resp => {
          //        if (resp) {
          //          let facebookToken = (resp.find(c => c.idSignupPlayerType == 2)) ? resp.find(c => c.idSignupPlayerType == 2).token : '';
          //          let googleToken = resp.find(c => c.idSignupPlayerType == 3) ? resp.find(c => c.idSignupPlayerType == 3).token : '';
          //          let config = new AuthServiceConfig(
          //            [
          //              {
          //                id: GoogleLoginProvider.PROVIDER_ID,
          //                provider: new GoogleLoginProvider(googleToken)
          //              },
          //              {
          //                id: FacebookLoginProvider.PROVIDER_ID,
          //                provider: new FacebookLoginProvider(facebookToken)
          //              }
          //            ]
          //          );
          //          this.authService = new AuthService(config);
          //        }
          //      }
          //    );
         }
       );
   }

   getCurrentLocation() {

     try {

       let apiLocation: {
         apiUrl: string;
         apiToken: string;
       }

       switch (this.apiLocationIntents) {
         case 0:
           apiLocation = {
             apiUrl: "https://ipinfo.io?token=",
             apiToken: "9d91c22fb3cfaa"
           };
           this.apiLocation(apiLocation);
           break;
         case 1:
           apiLocation = {
             apiUrl: "https://ipinfo.io?token=",
             apiToken: "b1d3b6760490a2"
           };
           this.apiLocation(apiLocation);
           break;
         case 2:
           apiLocation = {
             apiUrl: "https://ipinfo.io?token=",
             apiToken: "368c9afba0db23"
           };
           this.apiLocation(apiLocation);
           break;
         case 3:
           apiLocation = {
             apiUrl: "https://api.ipdata.co/?api-key=",
             apiToken: "b6b09cbe4c944e5a64194a2af9742a8e8817f06fb75ba61860d3db58"
           };
           this.apiLocation(apiLocation);
           break;
         default:
           this._cloudCountry = 'DF';
           this._ipAddress = '0.0.0.0';
           this._postal = '0000';
           this.getCurrency();
           this.getTitle();
           this.getCountry();
           this.getState(false);
           this.affiliateCookie = {
             affiliateCode: this.cookieService.get("affiliateCode") || null,
             bannerCode: this.cookieService.get("bannerCode") || null,
             campaignCode: this.cookieService.get("campaignCode") || null
           }
          //  this.signupService.getsocialNetworkToken(this._configData.jazzCoreApi, this._domain)
          //    .subscribe(
          //      resp => {
          //        if (resp) {
          //          let facebookToken = (resp.find(c => c.idSignupPlayerType == 2)) ? resp.find(c => c.idSignupPlayerType == 2).token : '';
          //          let googleToken = resp.find(c => c.idSignupPlayerType == 3) ? resp.find(c => c.idSignupPlayerType == 3).token : '';
          //          let config = new AuthServiceConfig(
          //            [
          //              {
          //                id: GoogleLoginProvider.PROVIDER_ID,
          //                provider: new GoogleLoginProvider(googleToken)
          //              },
          //              {
          //                id: FacebookLoginProvider.PROVIDER_ID,
          //                provider: new FacebookLoginProvider(facebookToken)
          //              }
          //            ]
          //          );
          //          this.authService = new AuthService(config);
          //        }
          //      }
          //    );
           break;
       }

     } catch{
       console.log("Error get current location");
     }

   }

   getColumnsBySiteCountryCurrency() {
     this.loading = true;
     try {
       let req = {
         siteTag: this._domain,
         idCountry: this._cloudCountry,
         idCurrency: this._currencySelected.idCurrency
       };
       this.signupService.getColumnsBySiteCountryCurrency(this._configData.jazzCoreApi, req)
         .subscribe(
           resp => {
             this._siteByPlayerForCountryCurrency = resp;
             if (this._siteByPlayerForCountryCurrency.find(c => c.name == 'Terms and Conditions')) {
               this.getTermsConditions(this._siteByPlayerForCountryCurrency[0].idSignUpPlayer);
               console.log(resp, "columns");
             }
           },
           error => {
             console.log("Error get columns");
           },
           () => {
             if (this._siteByPlayerForCountryCurrency.find(c => c.name == "Hear" || c.name == "Promotional Code" || c.name == "Referral Account")) {
               this.hearVisible = true;
             }
             if (this._siteByPlayerForCountryCurrency.find(c => c.name == "Sportsbook" || c.name == "Casino" || c.name == "Racebook" || c.name == 'Poker')) {
               this.interestsVisible = true;
             }
             this.loading = false;
           }
         );

     } catch{
       console.log('Error currency');
     }
   };

   getTitle() {
     try {
       this._title = this.translate.instant("signupColumns.titles");
     } catch{
       console.log('Error title');
     }
   };

   getCountry() {
     try {
       this.signupService.getCountry(this._configData.jazzCoreApi)
         .subscribe(
           resp => {
             this._country = resp;
           },
           error => {
             console.log("Error get country");
           }
         );
     } catch{
       console.log('Error country');
     }
   };

   getState(isoCountry: any) {
     try {
       if (isoCountry.value) {
         this.signupService.getState(this._configData.jazzCoreApi, isoCountry.value.idCountry)
           .subscribe(
             resp => {
               this._state = resp;
             },
             error => {
               console.log("Error get country");
             }
           );
       } else {
         this._state = null;
       }
     } catch{
       console.log('Error country');
     }
   };

   getCurrency() {
     try {
       this.signupService.getCurrenciesBySiteAndCountry(this._configData.jazzCoreApi, this._domain, this._cloudCountry)
         .subscribe(
           resp => {
             this._currency = resp;
             (resp.length > 1) ? this.currencyVisible = true : this.currencyVisible = false;
           },
           error => {
             console.log("Error Get Currency");
           },
           () => {
             this._currencySelected = this._currency[0];
             this.getColumnsBySiteCountryCurrency();
           }
         );
     } catch{
       console.log('Error country');
     }
   };

   //#endregion get

   //#region validate

   validatePassword(pass: string): boolean {
     try {
       if (this._siteByPlayerForCountryCurrency.find(c => c.name == 'Password') && this._siteByPlayerForCountryCurrency.find(c => c.name == 'Password').value && pass) {
         let passOriginal = this._siteByPlayerForCountryCurrency.find(c => c.name == 'Password');
         if (passOriginal && passOriginal.value.toLowerCase() == pass.toLocaleLowerCase()) {
           this.passwValid = true;
           return true;
         }
         else {
           this.passwValid = false;
           return false;
         }
       } else {
         if (!this._siteByPlayerForCountryCurrency.find(c => c.name == 'Password').value && !pass) {
           this.passwValid = true;
           return true;
         } else {
           this.passwValid = false;
           return false;
         }
       }

     } catch{
       console.log("Error validate password");
       return null;
     }

   };

   validateEmail(email: string): boolean {
     try {
       if (this._siteByPlayerForCountryCurrency.find(c => c.name == 'Email') && this._siteByPlayerForCountryCurrency.find(c => c.name == 'Email').value && email) {
         let emailOriginal = this._siteByPlayerForCountryCurrency.find(c => c.name == 'Email');

         if (emailOriginal && emailOriginal.value.toLocaleLowerCase() == email.toLocaleLowerCase()) {
           this.emailValid = true;
           return true;
         }
         else {
           this.emailValid = false;
           return false;
         }
       } else {
         this.emailValid = false;
         return false;
       }
     } catch{
       console.log("Error validate Email");
       return null;
     }

   };

   validateDate(): boolean {
     try {
       if (this._siteByPlayerForCountryCurrency.find(c => c.name == "Date of Birth") && this._siteByPlayerForCountryCurrency.find(c => c.name == "Date of Birth").value) {
         let dateOfBirth: Date = new Date(this._siteByPlayerForCountryCurrency.find(c => c.name == "Date of Birth").value);
         let birthMonth: number = dateOfBirth.getMonth() + 1;
         let birthDay: number = dateOfBirth.getDate();
         let birthYear: number = dateOfBirth.getFullYear();
         let x: string = "" + birthYear + "" + ((birthMonth > 9) ? "" + birthMonth : "0" + birthMonth) + ((birthDay > 9) ? "" + birthDay : "0" + birthDay);

         let currentMonth: number = this.date.getMonth() + 1;
         let currentDay: number = this.date.getDate();
         let currentYear: number = this.date.getFullYear();
         let y: number = currentYear * 10000 + currentMonth * 100 + currentDay * 1;
         if ((y - parseInt(x)) >= 180000) {
           return true;
         } else {
           return false;
         }
       } else {
         return true;
       }
     } catch{
       console.log("Error validate date");
       return null;
     }


   };

   validateEmailAnonymous(): boolean {
     try {
       if (this.emailAA && this.emailAAConfirm) {
         if (this.emailAA.toLocaleLowerCase() == this.emailAAConfirm.toLocaleLowerCase()) {
           this.emailAAValid = true;
           return true;
         }
         else {
           this.emailAAValid = false;
           return false;
         }
       } else {
         this.emailAAValid = false;
         return false;
       }
     } catch{
       console.log("Error validate Email");
       return null;
     }

   };

   //#endregion validate

   insertPlayer() {
     debugger
     try {
       this.loading = true;
       if (this.validateDate()) {
         if (this.passwValid == true) {
           if (this.emailValid == true) {

             var title: any;
             var country: any;
             var state: any;
             if (this._siteByPlayerForCountryCurrency.find(c => c.name == "Title")) {
               title = this._siteByPlayerForCountryCurrency.find(c => c.name == "Title").value;
             }
             if (this._siteByPlayerForCountryCurrency.find(c => c.name == "Country")) {
               country = this._siteByPlayerForCountryCurrency.find(c => c.name == "Country").value;
             }
             if (this._siteByPlayerForCountryCurrency.find(c => c.name == "State")) {
               state = this._siteByPlayerForCountryCurrency.find(c => c.name == "State").value;
             }
             this.player = {
               password: this._siteByPlayerForCountryCurrency.find(c => c.name == "Password") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Password").value : null,
               name: this._siteByPlayerForCountryCurrency.find(c => c.name == "First Name") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "First Name").value : null,
               lastName: this._siteByPlayerForCountryCurrency.find(c => c.name == "Last Name") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Last Name").value : null,
               lastName2: null,
               title: title ? title.title : 'Mr',
               address1: this._siteByPlayerForCountryCurrency.find(c => c.name == "Address") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Address").value : null,
               address2: this._siteByPlayerForCountryCurrency.find(c => c.name == "Address") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Address").value : null,
               city: this._siteByPlayerForCountryCurrency.find(c => c.name == "City") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "City").value : null,
               state: state ? state.idState : null,
               country: country ? country.idCountry : this._cloudCountry,
               zip: this._siteByPlayerForCountryCurrency.find(c => c.name == "ZIP") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "ZIP").value : this._postal,
               phone: this._siteByPlayerForCountryCurrency.find(c => c.name == "Phone Number") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Phone Number").value : null,
               fax: this._siteByPlayerForCountryCurrency.find(c => c.name == "Fax") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Fax").value : null,
               email: this._siteByPlayerForCountryCurrency.find(c => c.name == "Email") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Email").value : null,
               dateOfBirth: this._siteByPlayerForCountryCurrency.find(c => c.name == "Date of Birth") ? new Date(this._siteByPlayerForCountryCurrency.find(c => c.name == "Date of Birth").value) : null,
               signUpIP: this._ipAddress,
               secQuestion: this._siteByPlayerForCountryCurrency.find(c => c.name == "Sec Question") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Sec Question").value : null,
               secAnswer: this._siteByPlayerForCountryCurrency.find(c => c.name == "Sec Answer") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Sec Answer").value : null,
               idUserSocialNetwork: null,
               idSignupPlayersType: 1
             };
             this.validatePlayerInsert = {
               address: this.player.address1,
               email: this.player.email,
               firstName: this.player.name,
               phone: this.player.phone,
               siteDomain: this._domain,
               lastName: this.player.lastName,
               idLanguage: this.translate.store.currentLang,
               birthdate: this.player.dateOfBirth
             };
             this.signupService.validatePlayerInsert(this._configData.jazzCoreApi, this.validatePlayerInsert)
               .subscribe(
                 resp => {
                   if (resp.length > 0) {
                     this.loading = false;
                     this.toastr.warning(resp, this.translate.instant("signupMessages.warning"));
                   } else {
                     if (this._siteByPlayerForCountryCurrency.find(c => c.name == "Government ID") ||
                       this._siteByPlayerForCountryCurrency.find(c => c.name == "Passport") ||
                       this._siteByPlayerForCountryCurrency.find(c => c.name == "RFC") ||
                       this._siteByPlayerForCountryCurrency.find(c => c.name == "CURP")
                     ) {
                       this._playerIdentification = {
                         idPlayer: 0,
                         idNumber: this._siteByPlayerForCountryCurrency.find(c => c.name == "Government ID") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Government ID").value : null,
                         passport: this._siteByPlayerForCountryCurrency.find(c => c.name == "Passport") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Passport").value : null,
                         rfc: this._siteByPlayerForCountryCurrency.find(c => c.name == "RFC") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "RFC").value : null,
                         curp: this._siteByPlayerForCountryCurrency.find(c => c.name == "CURP") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "CURP").value : null
                       }
                     } else {
                       this._playerIdentification = null
                     };

                     this._insertCsiPlayer = {
                       accountNumber: null,
                       book: "",
                       firstName: this.player.name || "",
                       lastName: this.player.lastName || "",
                       ipAddress: this._ipAddress || "",
                       address: (this.player.address1 || ""),
                       city: (this.player.city || ""),
                       state: (this.player.state || ""),
                       zip: (this._postal || ""),
                       country: this._cloudCountry || "",
                       homePhone: "",
                       workPhone: "",
                       mobilePhone: (this.player.phone || ""),
                       fax: "",
                       textMessages: false,
                       email: this.player.email,
                       hear: this._siteByPlayerForCountryCurrency.find(c => c.name == "Hear") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Hear").value : '',
                       mother: "",
                       ssn: "",
                       remarks: "",
                       referralAccount: this._siteByPlayerForCountryCurrency.find(c => c.name == "Referral Account") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Referral Account").value : '',
                       bankingInfo: "",
                       sports: this.sportsBook,
                       casino: this.casino,
                       horses: this.racebook,
                       poker: this.poker,
                       userId: 1,
                       password: "",
                       feesOver: 0,
                       promoCode: this._siteByPlayerForCountryCurrency.find(c => c.name == "Promotional Code") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Promotional Code").value : null,
                       claimedBy: 0,
                     };

                     var req = {
                       reqSignUp: {
                         siteDomain: this._domain,
                         idCountry: this._cloudCountry,
                         idCurrency: this._currencySelected.idCurrency,
                         idLanguage: this.translate.store.currentLang
                       },
                       playerInsert: this.player,
                       playerIdentification: this._playerIdentification,
                       csiPlayer: this._insertCsiPlayer,
                       affiliates: (this.affiliateCookie.affiliateCode) ? this.affiliateCookie : null
                     };

                     req.reqSignUp.siteDomain = this._domain;

                     this.signupService.playerInsert(this._configData.jazzCoreApi, req)
                       .subscribe(
                         resp => {
                           if (resp) {
                             this.loading = false;
                             this._playerInfo = resp;
                             localStorage.setItem("player", JSON.stringify(this._playerInfo.idPlayer))
                             localStorage.setItem("site", JSON.stringify(this._domain));
                             this.toastr.success(this.translate.instant("signupMessages.newAccount"), this.translate.instant("signupMessages.success"));
                           } else {
                             this.loading = false;
                             this.toastr.error(this.translate.instant("signupMessages.errorNewAccount"), this.translate.instant("signupMessages.error"));
                           }
                         },
                         error => {
                           console.log("Error insert player. Player insert");
                         },
                         () => {
                           this._playerInfo ? this.router.navigate([this.translate.instant("routes.welcomeNewPlayer")]) : null;
                         }
                       );
                   }
                 },
                 error => {
                   console.log("Error insert player. Validate player insert");
                 }
               );
           } else {
             this.loading = false;
           }
         } else {
           this.loading = false;
         }
       } else {
         this.loading = false;
         this.toastr.error(this.translate.instant("signupMessages.older18"), this.translate.instant("signupMessages.error"));
       }
     } catch{
       console.log("Error insert player");
     }

   }

  //  socialSignIn(socialPlatform: string) {
  //    this.loading = true;
  //    let socialPlatformProvider;
  //    if (socialPlatform == "facebook") {
  //      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
  //      this.authService.signIn(socialPlatformProvider).then(
  //        (userDataFacebook) => {
  //          this.insertSocialPlayer(userDataFacebook.facebook.email, userDataFacebook.facebook.first_name, userDataFacebook.facebook.last_name, userDataFacebook.facebook.id, 2)
  //        }, (error: any) => {
  //          this.loading = false;
  //        }
  //      );
  //    } else
  //      if (socialPlatform == "google") {
  //        socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
  //        this.authService.signIn(socialPlatformProvider)
  //          .then(
  //            (userDataGoogle) => {
  //              this.insertSocialPlayer(userDataGoogle.email, userDataGoogle.firstName, userDataGoogle.lastName, userDataGoogle.id, 3)
  //            }
  //          );
  //      }
  //  }

   signOut(): void {
     this.authService.signOut();
   }

   insertSocialPlayer(email: string, first_name: string, last_name: string, idUserSocialNetwork: string, idSignupPlayersType: number) {
     try {

       this.player = {
         password: null,
         name: first_name,
         lastName: last_name,
         lastName2: "",
         title: "",
         address1: "",
         address2: "",
         city: "",
         state: "",
         country: this._cloudCountry,
         zip: this._postal,
         phone: "",
         fax: "",
         email: email,
         dateOfBirth: null,
         signUpIP: this._ipAddress,
         secQuestion: "",
         secAnswer: "",
         idUserSocialNetwork: idUserSocialNetwork,
         idSignupPlayersType: idSignupPlayersType
       };
       this.validatePlayerInsert = {
         address: this.player.address1,
         email: this.player.email,
         firstName: this.player.name,
         phone: this.player.phone,
         siteDomain: this._domain,
         lastName: this.player.lastName,
         idLanguage: this.translate.store.currentLang,
         birthdate: this.player.dateOfBirth
       };
       this.signupService.validatePlayerInsert(this._configData.jazzCoreApi, this.validatePlayerInsert)
         .subscribe(
           resp => {
             if (resp.length > 0) {
               this.loading = false;
               this.toastr.warning(resp, this.translate.instant("signupMessages.warning"));
             } else {
               if (this._siteByPlayerForCountryCurrency.find(c => c.name == "Government ID") ||
                 this._siteByPlayerForCountryCurrency.find(c => c.name == "Passport") ||
                 this._siteByPlayerForCountryCurrency.find(c => c.name == "RFC") ||
                 this._siteByPlayerForCountryCurrency.find(c => c.name == "CURP")
               ) {
                 this._playerIdentification = {
                   idPlayer: 0,
                   idNumber: this._siteByPlayerForCountryCurrency.find(c => c.name == "Government ID") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Government ID").value : null,
                   passport: this._siteByPlayerForCountryCurrency.find(c => c.name == "Passport") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "Passport").value : null,
                   rfc: this._siteByPlayerForCountryCurrency.find(c => c.name == "RFC") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "RFC").value : null,
                   curp: this._siteByPlayerForCountryCurrency.find(c => c.name == "CURP") ? this._siteByPlayerForCountryCurrency.find(c => c.name == "CURP").value : null
                 }
               } else {
                 this._playerIdentification = null
               };

               this._insertCsiPlayer = {
                 accountNumber: null,
                 book: "",
                 firstName: this.player.name || "",
                 lastName: this.player.lastName || "",
                 ipAddress: this._ipAddress || "",
                 address: (this.player.address1 || ""),
                 city: (this.player.city || ""),
                 state: (this.player.state || ""),
                 zip: (this._postal || ""),
                 country: this._cloudCountry || "",
                 homePhone: "",
                 workPhone: "",
                 mobilePhone: (this.player.phone || ""),
                 fax: "",
                 textMessages: false,
                 email: this.player.email,
                 hear: "",
                 mother: "",
                 ssn: "",
                 remarks: "",
                 referralAccount: '',
                 bankingInfo: "",
                 sports: this.sportsBook,
                 casino: this.casino,
                 horses: this.racebook,
                 poker: this.poker,
                 userId: 1,
                 password: "",
                 feesOver: 0,
                 promoCode: "",
                 claimedBy: 0,
               };

               var req = {
                 reqSignUp: {
                   siteDomain: this._domain,
                   idCountry: this._cloudCountry,
                   idCurrency: this._currencySelected.idCurrency,
                   idLanguage: this.translate.store.currentLang
                 },
                 playerInsert: this.player,
                 playerIdentification: this._playerIdentification,
                 csiPlayer: this._insertCsiPlayer
               };

               this.signupService.playerInsert(this._configData.jazzCoreApi, req)
                 .subscribe(
                   resp => {
                     if (resp) {
                       this.loading = false;
                       this._playerInfo = resp;
                       localStorage.setItem("player", JSON.stringify(this._playerInfo.idPlayer))
                       localStorage.setItem("site", JSON.stringify(this._domain));
                       this.toastr.success(this.translate.instant("signupMessages.newAccount"), this.translate.instant("signupMessages.success"));
                     } else {
                       this.loading = false;
                       this.toastr.error(this.translate.instant("signupMessages.errorNewAccount"), this.translate.instant("signupMessages.error"));
                     }
                   },
                   error => {
                     this.loading = false;
                     console.log("Error insert player. Player insert");
                   },
                   () => {
                     this._playerInfo ? this.router.navigate([this.translate.instant("routes.welcomeNewPlayer")]) : null;
                   }
                 );
             }
           },
           error => {
             this.loading = false;
             console.log("Error insert player. Validate player insert");
           }
         );
       this.signOut();

     } catch{
       this.signOut();
       console.log("Error insert player");
     }
   }

   confirmAnonymousAccount(status: boolean, form: NgForm) {
     form.reset();
     this._anonymousAccount = status;

   };

   hideAnonymousAccount(form: NgForm) {
     form.resetForm();
     this.sendingInfo = false;
   }

   insertAnonymousPlayer() {
     try {
       if (this.emailAAValid == true) {
         this.player = {
           password: '',
           name: '',
           lastName: '',
           lastName2: '',
           title: 'Mr',
           address1: '',
           address2: '',
           city: '',
           state: '',
           country: this._cloudCountry,
           zip: this._postal,
           phone: '',
           fax: '',
           email: this.emailAA,
           dateOfBirth: null,
           signUpIP: this._ipAddress,
           secQuestion: '',
           secAnswer: '',
           idUserSocialNetwork: '',
           idSignupPlayersType: 1,
         };
         this.validatePlayerInsert = {
           address: this.player.address1,
           email: this.player.email,
           firstName: this.player.name,
           phone: this.player.phone,
           siteDomain: this._domain,
           lastName: this.player.lastName,
           idLanguage: this.translate.store.currentLang,
           birthdate: this.player.dateOfBirth
         };
         this.signupService.validatePlayerInsert(this._configData.jazzCoreApi, this.validatePlayerInsert)
           .subscribe(
             resp => {
               if (resp.length > 0) {
                 this.toastr.warning(resp, this.translate.instant("signupMessages.warning"));
               } else {

                 this._playerIdentification = null

                 this._insertCsiPlayer = {
                   accountNumber: null,
                   book: "",
                   firstName: this.player.name,
                   lastName: this.player.lastName,
                   ipAddress: this._ipAddress,
                   address: (this.player.address1 || ""),
                   city: (this.player.city || ""),
                   state: (this.player.state || ""),
                   zip: (this._postal || ""),
                   country: this._cloudCountry,
                   homePhone: "",
                   workPhone: "",
                   mobilePhone: (this.player.phone || ""),
                   fax: "",
                   textMessages: false,
                   email: this.player.email,
                   hear: "",
                   mother: "",
                   ssn: "",
                   remarks: "",
                   referralAccount: "",
                   bankingInfo: "",
                   sports: this.sportsBook,
                   casino: this.casino,
                   horses: this.racebook,
                   poker: this.poker,
                   userId: 1,
                   password: "",
                   feesOver: 0,
                   promoCode: null,
                   claimedBy: 0,
                 };

                 var req = {
                   reqSignUp: {
                     siteDomain: this._domain,
                     idCountry: this._cloudCountry,
                     idCurrency: this._currencySelected.idCurrency,
                     idLanguage: this.translate.store.currentLang
                   },
                   playerInsert: this.player,
                   playerIdentification: this._playerIdentification,
                   csiPlayer: this._insertCsiPlayer
                 };

                 this.signupService.playerInsert(this._configData.jazzCoreApi, req)
                   .subscribe(
                     resp => {
                       if (resp) {
                         this._playerInfo = resp;
                         localStorage.setItem("player", JSON.stringify(this._playerInfo.idPlayer))
                         localStorage.setItem("site", JSON.stringify(this._domain));
                         this.toastr.success(this.translate.instant("signupMessages.newAccount"), this.translate.instant("signupMessages.success"));
                       } else {
                         this.toastr.error(this.translate.instant("signupMessages.errorNewAccount"), this.translate.instant("signupMessages.error"));
                       }
                     },
                     error => {
                       console.log("Error insert player. Player insert");
                     },
                     () => {
                       this._playerInfo ? this.router.navigate([this.translate.instant("routes.welcomeNewPlayer")]) : null;
                     }
                   );
               }
             },
             error => {
               console.log("Error insert player. Validate player insert");
             }
           );
       }
     } catch{
       console.log("Error insert player");
     }
   }

   getTermsConditions(idSiteTag: number) {
     this.signupService.getTermsConditions(this._configData.jazzCoreApi, idSiteTag)
       .subscribe(
         resp => {
           if (resp) {
             this.termsConditionsVisible = true;
             this.termsConditions = this.translate.instant(resp.pathLanguage);
           }
         }
       );
   }

   getImagesBySite(idSite: number) {
     try {
       this.signupService.getImagesBySite(this._configData.jazzCoreApi, idSite)
         .subscribe(
           resp => {
             this._urlImages = resp;
           },
           error => {
             console.log("Error get Title");
           }
         );
     } catch{
       console.log('Error title');
     }
   };

   getIdSiteByUrl() {
     try {
       this.signupService.getIdSite(this._configData.jazzCoreApi, this._domain)
         .subscribe(
           resp => {
             if (resp != null || resp != undefined) {
               this.getImagesBySite(resp)
             }
           },
           error => {
             console.log("Error get Title");
           }
         );
     } catch{
       console.log('Error title');
     }
   };

   showResendEmail() {
     this._resendEmailVisible = true;
   }

   resendEmailConfirmation() {
     this.sendingInfo = true;
     try {

       var req = {
         email: this._emailToResend,
         siteDomain: this._domain,
         idLanguage: this.translate.store.currentLang
       };

       this.signupService.resendEmailConfirmation(this._configData.jazzCoreApi, req)
         .subscribe(
           resp => {
             this.sendingInfo = false;
             if (resp) {
               this._resendEmailVisible = false;
               this.toastr.success(this.translate.instant("signupMessages.resendEmailSuccess"), this.translate.instant("signupMessages.success"));
             } else {
               this.toastr.error(this.translate.instant("signupMessages.resendEmailError"), this.translate.instant("signupMessages.error"));
             }
           },
           error => {
             this.sendingInfo = false;
           }
         )
     } catch (error) {
       this.sendingInfo = false;
     }

   }

   changeLang(language: string){
     this.translate.use(language);
   }

 }
