import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentsSignUpComponent } from './agents-sign-up.component';

describe('AgentsSignUpComponent', () => {
  let component: AgentsSignUpComponent;
  let fixture: ComponentFixture<AgentsSignUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgentsSignUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentsSignUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
