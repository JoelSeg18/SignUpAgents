import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Http, HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { ToastrModule } from 'ngx-toastr';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgxLoadingModule } from 'ngx-loading';

import { ConfigService } from './services/config.service';



import { AgentsSignUpComponent } from './components/agents/agents-sign-up/agents-sign-up.component';
// import { WelcomePlayerComponent } from './components/players/welcome-player/welcome-player.component';
import { EmailConfirmationComponent } from './components/agents/email-confirmation/email-confirmation.component';

import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";

import { CookieService } from 'ngx-cookie-service';
// import { SignupDealerSportsBookComponent } from './components/players/signup-dealer-sports-book/signup-dealer-sports-book.component';



export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}


export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider('686238869143-n1fu39ucg2gcndrqf50fj5a0dc29dg8p.apps.googleusercontent.com')
      },
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider('369585830447580')
      }
    ]
  );
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    AgentsSignUpComponent,
    // WelcomePlayerComponent,
    EmailConfirmationComponent,
    // SignupDealerSportsBookComponent,
  ],
  imports: [
    BrowserModule
    // , AccordionModule
    // , BrowserAnimationsModule
    , AppRoutingModule
    , HttpClientModule
    , FormsModule
    , SocialLoginModule
    , ToastrModule.forRoot()
    , HttpModule
    , TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgxLoadingModule.forRoot({})
  ],
  providers: [
    ConfigService,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }, CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
